package com.example.app;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPageAdapter extends FragmentPagerAdapter  {

    private final List<Fragment> ListFrag = new ArrayList<>();
    private final List<String> ListTittle = new ArrayList<>();
    private final List<String> ListTittleFull = new ArrayList<>();


    public ViewPageAdapter(FragmentManager fm) {
        super(fm);
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        return ListFrag.get(position);
    }

    @Override
    public int getCount() {
        return ListTittle.size();
    }


    public void addFragment(Fragment fragement, String Title) {
        ListFrag.add(fragement);
        ListTittle.add(Title);
    }


}