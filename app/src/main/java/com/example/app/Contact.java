package com.example.app;

public class Contact {

    private String Name;
    private String Phone;
    private String Mail;
    private String Adresse;
    private String LinkedIn;



    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }

    public String getLinkedIn() {
        return LinkedIn;
    }

    public void setLinkedIn(String linkedIn) {
        LinkedIn = linkedIn;
    }


    public Contact(String name, String phone, String mail,String adresse,String linkedIn) {
        Name = name;
        Phone = phone;
        Mail = mail;
        Adresse= adresse;
        LinkedIn =linkedIn;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String mail) {
        Mail = mail;
    }
}
