package com.example.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

public class Profil extends AppCompatActivity {
    Button btn_call;
    Button btn_map;
    String telephone;
    String adresse;
    String Linkadd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        //Definition des boutons
        btn_call = findViewById(R.id.btn_appele);
        btn_map = findViewById(R.id.btn_maps);


        getIntent();
        String name = getIntent().getStringExtra("name");
        telephone = getIntent().getStringExtra("telephone");
        adresse = getIntent().getStringExtra("adresse");
        Linkadd = getIntent().getStringExtra("Linkadd");
        String picture = getIntent().getStringExtra("image");
        ImageView Profil_picture = findViewById(R.id.imageView_contact);
        TextView contact = findViewById(R.id.Nom_contact_id);
        TextView phone = findViewById(R.id.telephone);
        TextView streetAd = findViewById(R.id.adresse);
        TextView Linkedin = findViewById(R.id.LinkedIn);
        phone.setText(telephone);
        streetAd.setText(adresse);
        contact.setText(name);
        Linkedin.setText(Linkadd);

        //Affichage de l'image
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.WHITE)
                .borderWidthDp(3)
                .cornerRadiusDp(30)
                .oval(false)
                .build();


        Picasso.get().load(picture).transform(transformation).into(Profil_picture);
        //Defintion des fonctionnalité bouton
        btn_call.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+telephone));
                startActivity(callIntent);
            }

        });
        btn_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adresse = getIntent().getStringExtra("adresse");
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + adresse);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }

        });

    }

}


