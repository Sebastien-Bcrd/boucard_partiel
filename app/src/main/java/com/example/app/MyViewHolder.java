package com.example.app;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

public class MyViewHolder extends RecyclerView.ViewHolder {

    TextView tvName;
    TextView tvMail;
    TextView tvPhone;
     LinearLayout item_contact;
     ImageView flech_touch;
    ImageView Profil_Picture ;
    private Example eleve;
    Button btn_call;
    Button btn_map;

    public MyViewHolder(@NonNull View itemView) {
        super(itemView);

        tvName = itemView.findViewById(R.id.contact_name);
        tvPhone = itemView.findViewById(R.id.contact_number);
        tvMail = itemView.findViewById(R.id.contact_mail);
        Profil_Picture=itemView.findViewById(R.id.profil_picture);

        flech_touch=itemView.findViewById(R.id.go_profil);
         btn_call = itemView.findViewById(R.id.btn_appele);
         btn_map = itemView.findViewById(R.id.btn_maps);

    }
    void display(Example eleve){
        this.eleve = eleve;
        tvName.setText(eleve.getName().getFirst()+' '+ eleve.getName().getLast());
        tvMail.setText(eleve.getEmail());
        tvPhone.setText(eleve.getPhone());
        String imageUrl = eleve.getPicture();
        //Loading image using Picasso
        final int radius = 5;
        final int margin = 5;
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.WHITE)
                .borderWidthDp(3)
                .cornerRadiusDp(30)
                .oval(false)
                .build();


        Picasso.get().load(imageUrl).transform(transformation).into(Profil_Picture);


    }

}
