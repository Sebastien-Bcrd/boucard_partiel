package com.example.app;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;


import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {
    private ViewPager pager;
    private ViewPageAdapter Vadapter;
    RecyclerViewAdapter recyclerAdapter;
    private List<Contact>lContact;
    private List<Example>lEleves;

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        //Definition du Retrofit


        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://android.busin.fr")
                .addConverterFactory(GsonConverterFactory.create());

        //Création d'un objet rétrofit
        Retrofit retrofit = builder.build();
        Contact_Client client=retrofit.create(Contact_Client.class);
        //On effectue le call
        Call<List<Example>> call = client.Contact_Info_total();
        call.enqueue(new Callback<List<Example>>() {
            @Override
            public void onResponse(Call<List<Example>> call, Response<List<Example>> response) {

                lEleves = response.body();

                RecyclerView recyclerView= findViewById(R.id.Contact_Rec);
                recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                recyclerView.setAdapter( new RecyclerViewAdapter(MainActivity.this,lEleves));
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

            }

            @Override
            public void onFailure(Call<List<Example>> call, Throwable t) {
                Log.d("faillure",t.getMessage());
            }
        });


        //Definition du recyclerAdapter





    }
     public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
         MenuItem menuItem = menu.findItem(R.id.action_search);

         SearchView searchView = (SearchView) menuItem.getActionView();
         searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
             @Override
             public boolean onQueryTextSubmit(String query) {
                 return false;
             }

             @Override
             public boolean onQueryTextChange(String newText) {
                 //recyclerAdapter.getFilter().filter(newText);
                 return false;
             }
         });


         return super.onCreateOptionsMenu(menu);
     }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.pop_dev) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Application développée par Sébastien BOUCARD");
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}



    //Definition du SearchView



