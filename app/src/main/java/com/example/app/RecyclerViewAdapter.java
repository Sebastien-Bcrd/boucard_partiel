package com.example.app;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<MyViewHolder> {

   private  Context mContext ;
    private List<Example> mContact;
    private List<Example> mContactCopie;
    private  List<Example> ListFragFull ;


    public RecyclerViewAdapter(Context mContext, List<Example> mContact) {
        this.mContext = mContext;
        this.mContact = mContact;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View v ;

       v= LayoutInflater.from(mContext).inflate(R.layout.item_profil,parent,false);
        final MyViewHolder vHolder = new MyViewHolder(v);



       return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.display(mContact.get(position));

        holder.flech_touch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, Profil.class);
                String name = mContact.get(position).getName().getFirst() + ' ' + mContact.get(position).getName().getLast();
                String telephone = mContact.get(position).getPhone();
                String adresse = mContact.get(position).getAddress().getCity() + "\n" +
                        mContact.get(position).getAddress().getState() + "\n" +
                        mContact.get(position).getAddress().getStreet();
                String LinkedIn = mContact.get(position).getLinkedin();
                String pictureUrl = mContact.get(position).getPicture();
                intent.putExtra("name", name);
                intent.putExtra("telephone", telephone);
                intent.putExtra("adresse", adresse);
                intent.putExtra("image", pictureUrl);
                intent.putExtra("LinkedIn", LinkedIn);

                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mContact.size();
    }

}
